package de.fuchspfoten.vollbeschaeftigung.model.dataobject;

import de.fuchspfoten.vollbeschaeftigung.model.Data;
import de.fuchspfoten.vollbeschaeftigung.model.Evaluatable;
import de.fuchspfoten.vollbeschaeftigung.model.KeyTransformable;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Represents a stack-based reverse polish notation formula.
 */
@SerializableAs("dfv.do.Formula")
public class Formula implements ConfigurationSerializable, Evaluatable<Number>, KeyTransformable<Formula> {

    /**
     * Reserved keywords which are not transformed during a key transform.
     */
    private static final Map<String, Function<Deque<Number>, Number>> keywords = new HashMap<>();

    static {
        keywords.put("+", stack -> stack.pop().doubleValue() + stack.pop().doubleValue());
        keywords.put("-", stack -> stack.pop().doubleValue() - stack.pop().doubleValue());
        keywords.put("/", stack -> stack.pop().doubleValue() / stack.pop().doubleValue());
        keywords.put("*", stack -> stack.pop().doubleValue() * stack.pop().doubleValue());
    }

    /**
     * The instructions for this formula.
     */
    private final List<Object> instructions;

    /**
     * Deserialization constructor.
     *
     * @param source Source map.
     */
    @SuppressWarnings("unchecked")
    public Formula(final Map<String, Object> source) {
        instructions = (List<Object>) source.get("def");
    }

    /**
     * Constructor.
     */
    private Formula() {
        instructions = new ArrayList<>();
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new HashMap<>();
        result.put("def", instructions);
        return result;
    }

    @Override
    public Formula transformKeys(final Function<String, String> keyTransformer) {
        final Formula formula = new Formula();
        for (final Object insn : instructions) {
            if (insn instanceof String && !keywords.containsKey(insn)) {
                formula.instructions.add(keyTransformer.apply((String) insn));
            } else {
                formula.instructions.add(insn);
            }
        }
        return formula;
    }

    @Override
    public Number evaluate(final Data context) {
        final Deque<Number> stack = new ArrayDeque<>();

        for (final Object insn : instructions) {
            if (insn instanceof Number) {
                stack.push((Number) insn);
            } else if (insn instanceof String) {
                final String insnStr = (String) insn;
                if (keywords.containsKey(insnStr)) {
                    stack.push(keywords.get(insnStr).apply(stack));
                } else {
                    final Number num = context.getEntry(insnStr, Number.class);
                    if (num == null) {
                        throw new IllegalArgumentException("unset key for formula: " + insnStr);
                    }

                    stack.push(num);
                }
            }
        }

        if (stack.isEmpty()) {
            throw new IllegalStateException("stack for formula empty");
        }
        return stack.pop();
    }

    @Override
    public List<String> getRequiredKeys() {
        return instructions.stream()
                .filter(x -> x instanceof String)
                .map(x -> (String) x)
                .filter(x -> !keywords.containsKey(x))
                .collect(Collectors.toList());
    }
}
