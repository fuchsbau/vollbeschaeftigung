package de.fuchspfoten.vollbeschaeftigung.model.dataobject.task;

import de.fuchspfoten.vollbeschaeftigung.model.KeyTransformable;
import de.fuchspfoten.vollbeschaeftigung.model.dataobject.condition.Condition;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * A task that can be fulfilled by a player.
 */
@RequiredArgsConstructor
public class BaseTask implements Cloneable, ConfigurationSerializable, KeyTransformable<BaseTask> {

    /**
     * The ID of the task.
     */
    private @Getter final String id;

    /**
     * The requirement needed to be fulfilled before this task can be attempted.
     */
    private @Getter Condition requirement;

    /**
     * Deserialization constructor.
     *
     * @param source Source map.
     */
    public BaseTask(final Map<String, Object> source) {
        id = (String) source.get("id");
        requirement = (Condition) source.get("requirement");
    }

    @Override
    protected BaseTask clone() throws CloneNotSupportedException {
        return (BaseTask) super.clone();
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("requirement", requirement);
        return result;
    }

    @Override
    public BaseTask transformKeys(final Function<String, String> keyTransformer) {
        if (!(requirement instanceof KeyTransformable<?>)) {
            return this;
        }

        try {
            //noinspection unchecked
            final KeyTransformable<? extends Condition> req = (KeyTransformable<? extends Condition>) requirement;
            final BaseTask task = clone();
            task.requirement = req.transformKeys(keyTransformer);
            return task;
        } catch (final CloneNotSupportedException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
