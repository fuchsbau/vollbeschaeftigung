package de.fuchspfoten.vollbeschaeftigung.model.dataobject.reward;

import de.fuchspfoten.vollbeschaeftigung.model.Job;
import org.bukkit.entity.Player;

/**
 * A reward that can be granted to a player.
 */
public interface Reward {

    /**
     * Grants the reward to the given player.
     *
     * @param target The player to grant the reward to.
     * @param job    The job in whose context the reward is granted.
     */
    void grant(Player target, Job job);
}
