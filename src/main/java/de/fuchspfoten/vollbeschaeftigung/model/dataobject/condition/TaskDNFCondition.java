package de.fuchspfoten.vollbeschaeftigung.model.dataobject.condition;

import de.fuchspfoten.vollbeschaeftigung.model.Job;
import lombok.RequiredArgsConstructor;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents a condition regarding tasks. Its formula is given as a DNF of task ID literals.
 */
@RequiredArgsConstructor
@SerializableAs("dfv.condition.TaskDNFCondition")
public class TaskDNFCondition implements Condition, ConfigurationSerializable {

    /**
     * The list of clauses of literals.
     */
    private final List<List<String>> clauses;

    /**
     * Deserialization constructor.
     *
     * @param source Source map.
     */
    @SuppressWarnings("unchecked")
    public TaskDNFCondition(final Map<String, Object> source) {
        clauses = (List<List<String>>) source.get("clauses");
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new HashMap<>();
        result.put("clauses", clauses);
        return result;
    }

    @Override
    public boolean check(final Player target, final Job job) {
        // Any of the clauses has to be satisfied.
        for (final List<String> clause : clauses) {
            boolean clauseSatisfied = true;

            // For a clause to be satisfied, all literals (task IDs) have to be true (completed).
            for (final String taskId : clause) {
                // If a literal is false, we can ignore the clause.
                if (!job.isTaskCompleted(taskId)) {
                    clauseSatisfied = false;
                    break;
                }
            }

            // If a clause is satisfied, the DNF evaluates to true.
            if (clauseSatisfied) {
                return true;
            }
        }

        // If no clause is satisfied, the DNF evaluates to false.
        return false;
    }
}
