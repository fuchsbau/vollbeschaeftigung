package de.fuchspfoten.vollbeschaeftigung.model.dataobject.reward;

import de.fuchspfoten.vollbeschaeftigung.model.Job;
import de.fuchspfoten.vollbeschaeftigung.model.KeyTransformable;
import lombok.RequiredArgsConstructor;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * A reward that grants money to a player.
 */
@RequiredArgsConstructor
@SerializableAs("dfv.reward.MoneyReward")
public class MoneyReward implements ConfigurationSerializable, KeyTransformable<MoneyReward>, Reward {

    /**
     * The data key for the percentage of the remaining money quota.
     */
    private final String percentKey;

    /**
     * The data key for the minimum money reward.
     */
    private final String minKey;

    /**
     * The data key for the maximum money reward.
     */
    private final String maxKey;

    /**
     * The data key for the money reward variance.
     */
    private final String varianceKey;

    /**
     * Deserialization constructor.
     *
     * @param source Source map.
     */
    public MoneyReward(final Map<String, Object> source) {
        percentKey = (String) source.get("percent");
        minKey = (String) source.get("min");
        maxKey = (String) source.get("max");
        varianceKey = (String) source.get("variance");
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new HashMap<>();
        result.put("percent", percentKey);
        result.put("min", minKey);
        result.put("max", maxKey);
        result.put("variance", varianceKey);
        return result;
    }

    @Override
    public MoneyReward transformKeys(final Function<String, String> keyTransformer) {
        return new MoneyReward(keyTransformer.apply(percentKey), keyTransformer.apply(minKey),
                keyTransformer.apply(maxKey), keyTransformer.apply(varianceKey));
    }

    @Override
    public void grant(final Player target, final Job job) {
        // TODO
    }
}
