package de.fuchspfoten.vollbeschaeftigung.model.dataobject.condition;

import de.fuchspfoten.vollbeschaeftigung.model.Job;
import org.bukkit.entity.Player;

/**
 * Represents a condition that can be evaluated in a data context.
 */
public interface Condition {

    /**
     * Checks the condition for the given target in the given context.
     *
     * @param target The target player.
     * @param job    The job in whose context the condition is checked.
     * @return The evaluation result.
     */
    boolean check(Player target, Job job);
}
