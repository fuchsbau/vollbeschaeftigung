package de.fuchspfoten.vollbeschaeftigung.model.dataobject.condition;

import de.fuchspfoten.vollbeschaeftigung.model.Job;
import de.fuchspfoten.vollbeschaeftigung.model.KeyTransformable;
import lombok.RequiredArgsConstructor;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Represents a condition regarding the number of jobs on the job list.
 */
@RequiredArgsConstructor
@SerializableAs("dfv.condition.JobListCondition")
public class JobListCondition implements Condition, ConfigurationSerializable, KeyTransformable<JobListCondition> {

    /**
     * The data key for the minimum number of jobs.
     */
    private final String minKey;

    /**
     * The data key for the maximum number of jobs.
     */
    private final String maxKey;

    /**
     * Deserialization constructor.
     *
     * @param source Source map.
     */
    public JobListCondition(final Map<String, Object> source) {
        minKey = (String) source.get("min");
        maxKey = (String) source.get("max");
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new HashMap<>();
        result.put("min", minKey);
        result.put("max", maxKey);
        return result;
    }

    @Override
    public JobListCondition transformKeys(final Function<String, String> keyTransformer) {
        return new JobListCondition(keyTransformer.apply(minKey), keyTransformer.apply(maxKey));
    }

    @Override
    public boolean check(final Player target, final Job job) {
        // TODO
        return false;
    }
}
