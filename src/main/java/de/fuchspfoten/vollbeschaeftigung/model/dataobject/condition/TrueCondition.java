package de.fuchspfoten.vollbeschaeftigung.model.dataobject.condition;

import de.fuchspfoten.vollbeschaeftigung.model.Job;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * A condition that is always true.
 */
@SerializableAs("dfv.condition.TrueCondition")
public class TrueCondition implements Condition, ConfigurationSerializable {

    /**
     * Deserialization constructor.
     *
     * @param source The source map.
     */
    public TrueCondition(final Map<String, Object> source) {
        // Do nothing.
    }

    @Override
    public Map<String, Object> serialize() {
        return new HashMap<>();
    }

    @Override
    public boolean check(final Player target, final Job job) {
        return true;
    }
}
