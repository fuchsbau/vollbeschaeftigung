package de.fuchspfoten.vollbeschaeftigung.model.dataobject.task;

import de.fuchspfoten.fuchslib.util.Pair;
import org.bukkit.configuration.serialization.SerializableAs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * A task that involves delivering items to an NPC.
 */
@SerializableAs("dfv.task.NPCDeliveryTask")
public class NPCDeliveryTask extends BaseTask {

    /**
     * The delivery key of this task. Used in NPC scripts.
     */
    private final String key;

    /**
     * The required items for this task. Entries consist of an item key and an amount key.
     */
    private final Collection<Pair<String, String>> items = new ArrayList<>();

    /**
     * Deserialization constructor.
     *
     * @param source Source map.
     */
    @SuppressWarnings("unchecked")
    public NPCDeliveryTask(final Map<String, Object> source) {
        super(source);
        key = (String) source.get("key");

        final Iterable<Map<String, Object>> itemList = (Iterable<Map<String, Object>>) source.get("items");
        for (final Map<String, Object> entry : itemList) {
            items.add(new Pair<>((String) entry.get("key"), (String) entry.get("amount")));
        }
    }

    @Override
    public NPCDeliveryTask clone() throws CloneNotSupportedException {
        return (NPCDeliveryTask) super.clone();
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = super.serialize();
        result.put("key", key);

        final Collection<Map<String, Object>> itemList = new ArrayList<>();
        for (final Pair<String, String> item : items) {
            final Map<String, Object> map = new HashMap<>();
            map.put("key", item.getFirst());
            map.put("amount", item.getSecond());
            itemList.add(map);
        }
        result.put("items", itemList);

        return result;
    }
}
