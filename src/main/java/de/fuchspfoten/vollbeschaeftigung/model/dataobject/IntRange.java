package de.fuchspfoten.vollbeschaeftigung.model.dataobject;

import de.fuchspfoten.vollbeschaeftigung.model.Data;
import de.fuchspfoten.vollbeschaeftigung.model.Evaluatable;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Represents an integer range.
 */
@SerializableAs("dfv.do.IntRange")
public class IntRange implements ConfigurationSerializable, Evaluatable<Integer> {

    /**
     * The random number generator.
     */
    private static final Random random = new Random();

    /**
     * The lower bound, inclusive.
     */
    private final int min;

    /**
     * The upper bound, inclusive.
     */
    private final int max;

    /**
     * Deserialization constructor.
     *
     * @param source Source map.
     */
    public IntRange(final Map<String, Object> source) {
        min = (int) source.get("min");
        max = (int) source.get("max");
    }

    /**
     * Constructor.
     *
     * @param min The lower bound.
     * @param max The upper bound.
     */
    private IntRange(final int min, final int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public Integer evaluate(final Data context) {
        return random.nextInt(max - min + 1) + min;
    }

    @Override
    public List<String> getRequiredKeys() {
        return Collections.emptyList();
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new LinkedHashMap<>();
        result.put("min", min);
        result.put("max", max);
        return result;
    }
}
