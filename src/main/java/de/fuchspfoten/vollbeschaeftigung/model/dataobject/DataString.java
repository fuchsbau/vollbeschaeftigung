package de.fuchspfoten.vollbeschaeftigung.model.dataobject;

import de.fuchspfoten.vollbeschaeftigung.model.Data;
import de.fuchspfoten.vollbeschaeftigung.model.Evaluatable;
import de.fuchspfoten.vollbeschaeftigung.model.KeyTransformable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

/**
 * A string that is instantiable, i.e. depends on instance context.
 */
public final class DataString implements Evaluatable<String>, KeyTransformable<DataString> {

    /**
     * Parses a dataold string from the given pattern string.
     *
     * @param pattern The pattern string.
     * @return The dataold string.
     */
    public static DataString parse(final String pattern) {
        final DataString result = new DataString();
        final StringBuilder builder = new StringBuilder();

        for (final char c : pattern.toCharArray()) {
            if (c == '{') {
                // Flush to string mode, skip, now in key mode.
                result.stringParts.add(builder.toString());
                builder.setLength(0);
            } else if (c == '}') {
                // Flush to key mode, skip, now in string mode.
                result.keyParts.add(builder.toString());
                builder.setLength(0);
            } else {
                // Keep.
                builder.append(c);
            }
        }

        // Pattern ends in string mode assuming all braces are closed.
        result.stringParts.add(builder.toString());

        return result;
    }

    /**
     * Raw string parts.
     */
    private final List<String> stringParts = new ArrayList<>();

    /**
     * Instance references.
     */
    private final List<String> keyParts = new ArrayList<>();

    /**
     * Constructor.
     */
    private DataString() {
    }

    /**
     * Converts the dataold string to a pattern string.
     *
     * @return The pattern string.
     */
    public String toPattern() {
        final StringBuilder builder = new StringBuilder();
        for (int i = 0; i < stringParts.size(); i++) {
            builder.append(stringParts.get(i));
            if (i < keyParts.size()) {
                builder.append('{').append(keyParts.get(i)).append('}');
            }
        }
        return builder.toString();
    }

    @Override
    public DataString transformKeys(final Function<String, String> keyTransformer) {
        final DataString result = new DataString();
        result.stringParts.addAll(stringParts);
        keyParts.stream().map(keyTransformer).forEach(result.keyParts::add);
        return result;
    }

    @Override
    public String evaluate(final Data context) {
        final StringBuilder builder = new StringBuilder();
        for (int i = 0; i < stringParts.size(); i++) {
            builder.append(stringParts.get(i));
            if (i < keyParts.size()) {
                builder.append(context.getEntry(keyParts.get(i), Object.class));
            }
        }
        return builder.toString();
    }

    @Override
    public List<String> getRequiredKeys() {
        return Collections.unmodifiableList(keyParts);
    }
}
