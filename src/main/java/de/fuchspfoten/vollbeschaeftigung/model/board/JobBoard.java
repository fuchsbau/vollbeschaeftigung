package de.fuchspfoten.vollbeschaeftigung.model.board;

import de.fuchspfoten.vollbeschaeftigung.VollbeschaeftigungPlugin;
import de.fuchspfoten.vollbeschaeftigung.model.Data;
import de.fuchspfoten.vollbeschaeftigung.model.Job;
import de.fuchspfoten.vollbeschaeftigung.model.JobPattern;
import de.fuchspfoten.vollbeschaeftigung.model.table.Table;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * A job board draws job patterns from a pool.
 */
@SerializableAs("dfv.JobBoard")
public class JobBoard implements ConfigurationSerializable {

    /**
     * A random number generator for this job board.
     */
    private static final Random random = new Random();

    /**
     * The ID of the board.
     */
    private @Getter final String boardId;

    /**
     * The minimum number of jobs on the board.
     */
    private final int minJobs;

    /**
     * The maximum number of jobs on the board.
     */
    private final int maxJobs;

    /**
     * The table which can be used for picking a job pattern from this board's pool.
     */
    private final Table poolTable;

    /**
     * The array of listed jobs. Size of this array is 54 (size of a double chest) at all times.
     */
    private final JobBoardEntry[] listedJobs = new JobBoardEntry[54];

    /**
     * The inventory that is used for displaying jobs.
     */
    private final Inventory boardInventory;

    /**
     * The number of listed jobs.
     */
    private int numListed = 0;

    /**
     * Deserialization constructor.
     *
     * @param source Source map.
     */
    public JobBoard(final Map<String, Object> source) {
        boardId = (String) source.get("id");
        minJobs = (Integer) source.get("min");
        maxJobs = (Integer) source.get("max");
        poolTable = (Table) source.get("pool");

        boardInventory = Bukkit.createInventory(null, 54, "");
    }

    /**
     * Opens this board for the given player.
     *
     * @param player The player.
     */
    public void openBoard(final HumanEntity player) {
        updateBoard();
        player.openInventory(boardInventory);
    }

    /**
     * Performs an update of the board, i.e. pruning expired jobs and initializing the board if needed.
     */
    private void updateBoard() {
        pruneJobs();

        // Initialize if there are jobs missing.
        if (numListed < minJobs) {
            generateJobs();
        }
    }

    /**
     * Prunes expired jobs from the board and replace (some) expired jobs with new ones.
     */
    private void pruneJobs() {
        boolean didRemove = false;
        for (int i = 0; i < listedJobs.length; i++) {
            if (listedJobs[i] != null && listedJobs[i].isExpired()) {
                listedJobs[i] = null;
                didRemove = true;
            }
        }

        if (didRemove) {
            generateJobs();
        }
    }

    /**
     * Generates jobs on the job board.
     */
    private void generateJobs() {
        // Fill to at least the minimum amount.
        while (numListed < minJobs) {
            generateJob();
        }

        // With a chance of 33% each: Add no jobs, add one job, or add two jobs, if the respective actions are allowed.
        // In the case that jobs are generated after a job is removed, this produces a random walk.
        if (numListed + 1 < maxJobs) {  // Two more jobs are possible.
            if (random.nextDouble() * 3 < 1) {
                generateJob();
                generateJob();
            } else {
                if (random.nextBoolean()) {
                    generateJob();
                }
            }
        } else if (numListed < maxJobs) {  // One more job is possible.
            if (random.nextBoolean()) {
                generateJob();
            }
        }  // else: Add no jobs.
    }

    /**
     * Generates a single job on the job board.
     */
    private void generateJob() {
        if (numListed >= listedJobs.length) {
            throw new IllegalStateException("can't generate jobs with full list");
        }

        // Use the pool to pick a pattern.
        final Data data = new Data();
        poolTable.pick(data);
        data.evaluateData();

        final String patternId = data.getEntry("pattern", String.class);
        final JobPattern pattern = VollbeschaeftigungPlugin.getSelf().getPatternLibrary().load(patternId);
        if (pattern == null) {
            throw new IllegalStateException("unknown pattern with id " + patternId + " on board");
        }
        final Job job = new Job(pattern);

        // Attempt placement of the job.
        final List<Integer> freeSpaces = new ArrayList<>();
        for (int i = 0; i < listedJobs.length; i++) {
            if (listedJobs[i] == null) {
                freeSpaces.add(i);
            }
        }

        final int choice = freeSpaces.get(random.nextInt(freeSpaces.size()));
        final long keepTime = 1000L * 60 * (random.nextInt(46) + 45);
        listedJobs[choice] = new JobBoardEntry(job, System.currentTimeMillis() + keepTime);
        boardInventory.setItem(choice, job.getListItem());
        numListed++;
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new HashMap<>();
        result.put("id", boardId);
        result.put("min", minJobs);
        result.put("max", maxJobs);
        result.put("pool", poolTable);
        return result;
    }
}
