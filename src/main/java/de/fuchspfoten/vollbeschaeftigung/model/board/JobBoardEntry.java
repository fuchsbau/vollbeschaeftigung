package de.fuchspfoten.vollbeschaeftigung.model.board;

import de.fuchspfoten.vollbeschaeftigung.model.Job;
import lombok.RequiredArgsConstructor;

/**
 * Represents an entry on a job board.
 */
@RequiredArgsConstructor
public class JobBoardEntry {

    /**
     * The job in the entry.
     */
    private final Job job;

    /**
     * The timestamp at which this entry will be removed.
     */
    private final long timeout;

    /**
     * Checks whether this entry has expired.
     *
     * @return {@code true} iff this entry has expired.
     */
    public boolean isExpired() {
        return System.currentTimeMillis() >= timeout;
    }
}
