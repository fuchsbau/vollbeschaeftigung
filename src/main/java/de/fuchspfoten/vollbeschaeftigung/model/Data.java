package de.fuchspfoten.vollbeschaeftigung.model;

import de.fuchspfoten.fuchslib.util.graph.Graph;
import de.fuchspfoten.fuchslib.util.graph.KeyGraph;
import de.fuchspfoten.fuchslib.util.graph.Node;
import de.fuchspfoten.fuchslib.util.graph.algorithm.VertexSorting;
import de.fuchspfoten.vollbeschaeftigung.model.dataobject.DataString;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

/**
 * Represents a set of data.
 */
@SerializableAs("dfv.Data")
public class Data implements ConfigurationSerializable {

    /**
     * Converts entries to other types, if required.
     *
     * @param input The input object.
     * @return The output object.
     */
    private static Object convertEntry(final Object input) {
        if (input instanceof String) {
            return DataString.parse((String) input);
        }
        return input;
    }

    /**
     * The internal store of data.
     */
    private final Map<String, Object> data;

    /**
     * The current aliases under which elements are added.
     */
    private final Deque<String> aliases;

    /**
     * Keeps track on whether the data has been evaluated.
     */
    private boolean evaluated;

    /**
     * Deserialization constructor.
     *
     * @param source Source map.
     */
    @SuppressWarnings("unchecked")
    public Data(final Map<String, Object> source) {
        data = (Map<String, Object>) source.get("data");

        // Aliases are not serialized as a data storage should not be serialized during evaluation, which is the only
        // time when aliases are in use.
        aliases = new ArrayDeque<>();
    }

    /**
     * Constructor.
     */
    public Data() {
        data = new HashMap<>();
        aliases = new ArrayDeque<>();
    }

    /**
     * Enters the given source entry into this data object.
     *
     * @param source The source entry.
     */
    public void enter(final Map<String, Object> source) {
        for (final Entry<String, Object> entry : source.entrySet()) {
            final Object oldValue = data.get(entry.getKey());
            final Object newValue = applyChanges(entry.getValue());

            if (!(oldValue instanceof List<?>) || !(newValue instanceof List<?>)) {
                // Overwrite or set entry. The case that oldValue == null is covered by the instanceof.
                data.put(entry.getKey(), newValue);
            } else {
                // Both entries are lists: insert new into old.
                try {
                    //noinspection unchecked
                    ((Collection) oldValue).addAll((Collection) newValue);
                } catch (final ClassCastException ex) {
                    // Ignore the exception and leave the old entry in place as the types are not compatible.
                }
            }
        }
    }

    /**
     * Retrieves the entry for the given key.
     *
     * @param key   The key.
     * @param clazz The expected result class.
     * @param <T>   The type of the expected result.
     * @return The result.
     */
    public <T> T getEntry(final String key, final Class<T> clazz) {
        if (!evaluated) {
            throw new IllegalStateException("Data not evaluated");
        }

        final Object obj = data.get(key);
        if (obj == null) {
            return null;
        }

        if (clazz.isInstance(obj)) {
            return clazz.cast(obj);
        }
        return null;
    }

    /**
     * Pushes an alias to the alias stack.
     *
     * @param alias The alias.
     */
    public void pushAlias(final String alias) {
        // We push to the tail to make name generation (join of all elements in the stack) nicer.
        aliases.addLast(alias);
    }

    /**
     * Removes the last element in the alias stack.
     */
    public void popAlias() {
        // We push to the tail to make name generation (join of all elements in the stack) nicer.
        aliases.pollLast();
    }

    /**
     * Evaluates the data set, respecting the requirement order.
     */
    public void evaluateData() {
        // We set this first to allow carefully orchestrated access via getEntry during evaluation.
        evaluated = true;

        // Construct a graph for the data keys.
        final Graph<String> requirementGraph = new KeyGraph<>();
        data.keySet().stream().map(Node::new).forEach(requirementGraph::addNode);

        // Extract data for the requirement relation.
        final Map<String, Collection<String>> requirementData = new HashMap<>();
        data.entrySet().stream()
                .filter(e -> e.getValue() instanceof Evaluatable)
                .forEach(e -> requirementData.put(e.getKey(), ((Evaluatable<?>) e.getValue()).getRequiredKeys()));

        // Create edges from the requirement relation.
        for (final Entry<String, Collection<String>> entry : requirementData.entrySet()) {
            final Node<String> baseNode = requirementGraph.getNodeWith(entry.getKey());
            for (final String requiredKey : entry.getValue()) {
                requirementGraph.getNodeWith(requiredKey).addAdjacentNode(baseNode);
            }
        }

        // Obtain the evaluation order by topologically sorting the graph nodes.
        final List<Node<String>> evaluationOrder = VertexSorting.topologicalSorting(requirementGraph);

        // Evaluate the data in the obtained order.
        for (final Node<String> node : evaluationOrder) {
            final Object dataEntry = data.get(node.getPayload());
            if (dataEntry instanceof Evaluatable<?>) {
                data.put(node.getPayload(), ((Evaluatable) dataEntry).evaluate(this));
            }
        }
    }

    /**
     * Transforms the keys of input objects, if possible.
     *
     * @param input The input object.
     * @return The output object.
     */
    private Object transformKeys(final Object input) {
        if (aliases.isEmpty()) {
            return input;
        }

        if (input instanceof KeyTransformable) {
            return ((KeyTransformable) input).transformKeys(s -> s + String.join("_", aliases));
        }
        return input;
    }

    /**
     * Applies changes from configuration object to data object.
     *
     * @param input The input object.
     * @return The output object.
     */
    private Object applyChanges(final Object input) {
        if (input instanceof List<?>) {
            return ((Collection<?>) input).stream()
                    .map(Data::convertEntry)
                    .map(this::transformKeys)
                    .collect(Collectors.toList());
        }
        return transformKeys(convertEntry(input));
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new HashMap<>();
        result.put("data", data);
        return result;
    }
}
