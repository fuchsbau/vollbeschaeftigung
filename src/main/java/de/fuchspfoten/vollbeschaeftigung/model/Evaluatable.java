package de.fuchspfoten.vollbeschaeftigung.model;

import java.util.List;

/**
 * Something that can be evaluated in a data context.
 */
public interface Evaluatable<T> {

    /**
     * Evaluate this object in the given data context.
     *
     * @param context The data context.
     * @return The result of evaluating this object.
     */
    T evaluate(Data context);

    /**
     * Returns the required keys for evaluating this object.
     *
     * @return The list of required keys.
     */
    List<String> getRequiredKeys();
}
