package de.fuchspfoten.vollbeschaeftigung.model.table;

import de.fuchspfoten.vollbeschaeftigung.model.Data;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

/**
 * Represents a data table.
 */
public interface Table extends ConfigurationSerializable {

    /**
     * Obtains the weight of this table.
     *
     * @return The weight of this table.
     */
    double getWeight();

    /**
     * Picks from the table.
     *
     * @param target The data object to pick into.
     */
    void pick(final Data target);
}
