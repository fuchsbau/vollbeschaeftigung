package de.fuchspfoten.vollbeschaeftigung.model.table;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Common logic for all tables.
 */
@RequiredArgsConstructor
public abstract class BaseTable implements Table {

    /**
     * The weight of the table.
     */
    private @Getter final double weight;

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new LinkedHashMap<>();

        // Only store weight if different from the default of 1.0 to save on space consumption.
        if (Math.abs(weight - 1.0) >= 0.001) {
            result.put("weight", weight);
        }

        return result;
    }
}
