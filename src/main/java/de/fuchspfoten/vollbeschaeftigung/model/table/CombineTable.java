package de.fuchspfoten.vollbeschaeftigung.model.table;

import de.fuchspfoten.vollbeschaeftigung.model.Data;
import org.bukkit.configuration.serialization.SerializableAs;

import java.util.Collection;
import java.util.Map;

/**
 * A table that contains possibly multiple tables.
 */
@SerializableAs("dfv.tab.CombineTable")
public class CombineTable extends BaseTable {

    /**
     * The contained tables.
     */
    private final Collection<Table> tables;

    /**
     * Deserialization constructor.
     *
     * @param source Source map.
     */
    @SuppressWarnings("unchecked")
    public CombineTable(final Map<String, Object> source) {
        super(((Number) source.getOrDefault("weight", 1.0)).doubleValue());
        tables = (Collection<Table>) source.get("tables");
    }

    @Override
    public void pick(final Data target) {
        tables.forEach(t -> t.pick(target));
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = super.serialize();
        result.put("tables", tables);
        return result;
    }
}
