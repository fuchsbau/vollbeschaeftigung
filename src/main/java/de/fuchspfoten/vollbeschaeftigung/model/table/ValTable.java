package de.fuchspfoten.vollbeschaeftigung.model.table;

import de.fuchspfoten.vollbeschaeftigung.model.Data;
import org.bukkit.configuration.serialization.SerializableAs;

import java.util.Map;

/**
 * A table that contains a single set of data.
 */
@SerializableAs("dfv.tab.ValTable")
public class ValTable extends BaseTable {

    /**
     * Creates a table for the given data set entry.
     *
     * @param entry The data set entry.
     * @return The created table.
     */
    public static ValTable forValue(final Map<String, Object> entry) {
        return new ValTable(1.0, entry);
    }

    /**
     * The unparsed data set entry.
     */
    private final Map<String, Object> entry;

    /**
     * Constructor.
     *
     * @param weight The weight of the table.
     * @param entry  The entry for the table.
     */
    private ValTable(final double weight, final Map<String, Object> entry) {
        super(weight);
        this.entry = entry;
    }

    /**
     * Deserialization constructor.
     *
     * @param source Source map.
     */
    @SuppressWarnings("unchecked")
    public ValTable(final Map<String, Object> source) {
        super(((Number) source.getOrDefault("weight", 1.0)).doubleValue());
        entry = (Map<String, Object>) source.get("val");
    }

    @Override
    public void pick(final Data target) {
        target.enter(entry);
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = super.serialize();
        result.put("val", entry);
        return result;
    }
}
