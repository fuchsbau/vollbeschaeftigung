package de.fuchspfoten.vollbeschaeftigung.model.table;

import de.fuchspfoten.vollbeschaeftigung.model.Data;
import org.bukkit.configuration.serialization.SerializableAs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Random;

/**
 * A table that contains a single set of data.
 */
@SerializableAs("dfv.tab.PickTable")
public class PickTable extends BaseTable {

    /**
     * The randomness source.
     */
    private static final Random random = new Random();

    /**
     * The contained tables.
     */
    private final Collection<Table> tables;

    /**
     * Deserialization constructor.
     *
     * @param source Source map.
     */
    @SuppressWarnings("unchecked")
    public PickTable(final Map<String, Object> source) {
        super(((Number) source.getOrDefault("weight", 1.0)).doubleValue());
        if (source.containsKey("values")) {
            tables = new ArrayList<>();
            final Collection<Map<String, Object>> dataEntries = (Collection<Map<String, Object>>) source.get("values");
            dataEntries.stream().map(ValTable::forValue).forEach(tables::add);
        } else {
            tables = (Collection<Table>) source.get("tables");
        }
    }

    @Override
    public void pick(final Data target) {
        // Pick a table using fitness proportionate selection.
        final double sum = tables.stream().map(Table::getWeight).reduce(0.0, Double::sum);
        final double pick = random.nextDouble() * sum;

        // Track the unchosen part of the imaginary wheel using the accumulator. Once the unchosen part is passed,
        // the selection has been identified.
        double accumulator = 0;
        for (final Table table : tables) {
            accumulator += table.getWeight();
            if (accumulator > pick) {
                table.pick(target);
                break;
            }
        }
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = super.serialize();
        result.put("tables", tables);
        return result;
    }
}
