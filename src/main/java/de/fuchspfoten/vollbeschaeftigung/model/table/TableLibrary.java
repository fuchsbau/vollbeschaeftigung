package de.fuchspfoten.vollbeschaeftigung.model.table;

import de.fuchspfoten.vollbeschaeftigung.model.BaseLibrary;

/**
 * The table library keeps the tables from the tables/ subdirectory.
 */
public class TableLibrary extends BaseLibrary<Table> {

    /**
     * Constructor.
     */
    public TableLibrary() {
        super(Table.class);
    }

    @Override
    public Table load(final String id) {
        return super.load(id);
    }
}
