package de.fuchspfoten.vollbeschaeftigung.model.table;

import de.fuchspfoten.vollbeschaeftigung.VollbeschaeftigungPlugin;
import de.fuchspfoten.vollbeschaeftigung.model.Data;
import org.bukkit.configuration.serialization.SerializableAs;

import java.util.Map;

/**
 * A table that references another table.
 */
@SerializableAs("dfv.tab.RefTable")
public class RefTable extends BaseTable {

    /**
     * The ID of the table that is referenced.
     */
    private final String id;

    /**
     * The alias under which the referenced table shall be imported.
     */
    private final String alias;

    /**
     * Deserialization constructor.
     *
     * @param source Source map.
     */
    public RefTable(final Map<String, Object> source) {
        super(((Number) source.getOrDefault("weight", 1.0)).doubleValue());
        id = (String) source.get("id");
        alias = (String) source.get("alias");
    }

    @Override
    public void pick(final Data target) {
        if (alias != null) {
            target.pushAlias(alias);
        }

        VollbeschaeftigungPlugin.getSelf().getTableLibrary().load(id).pick(target);

        if (alias != null) {
            target.popAlias();
        }
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = super.serialize();
        result.put("id", id);
        result.put("alias", alias);
        return result;
    }
}
