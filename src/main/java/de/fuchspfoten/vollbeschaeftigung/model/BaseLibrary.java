package de.fuchspfoten.vollbeschaeftigung.model;

import lombok.RequiredArgsConstructor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents a library of file-loaded objects.
 */
@RequiredArgsConstructor
public abstract class BaseLibrary<T extends ConfigurationSerializable> {

    /**
     * The id-to-object mapping that is used as the library.
     */
    private final Map<String, T> library = new HashMap<>();

    /**
     * The type of object that is stored.
     */
    private final Class<T> clazz;

    /**
     * Loads the object in the given file.
     *
     * @param file The file.
     */
    public void loadFile(final File file) {
        final YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

        if (!config.isSet("id")) {
            throw new IllegalArgumentException("id entry not set for file");
        }
        final String id = config.getString("id");

        if (!config.isSet("obj")) {
            throw new IllegalArgumentException("obj entry not set for file");
        }
        final T obj = config.getSerializable("obj", clazz);

        library.put(id, obj);
    }

    /**
     * Loads the object with the given ID from this library.
     *
     * @param id The ID.
     * @return The object, or {@code null}.
     */
    protected T load(final String id) {
        return library.get(id);
    }
}
