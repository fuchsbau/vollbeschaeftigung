package de.fuchspfoten.vollbeschaeftigung.model;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.item.ItemFactory;
import de.fuchspfoten.vollbeschaeftigung.VollbeschaeftigungPlugin;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

/**
 * A job is an instance of a {@link de.fuchspfoten.vollbeschaeftigung.model.JobPattern}.
 */
@SerializableAs("dfv.Job")
public class Job implements ConfigurationSerializable {

    /**
     * The ID of this job.
     */
    private final UUID id;

    /**
     * The underlying job pattern.
     */
    private final JobPattern pattern;

    /**
     * The data storage for the job.
     */
    private final Data data;

    /**
     * A list of completed tasks.
     */
    private final Collection<String> completedTasks;

    /**
     * Deserialization constructor.
     *
     * @param source Source map.
     */
    @SuppressWarnings("unchecked")
    public Job(final Map<String, Object> source) {
        id = UUID.fromString((String) source.get("id"));
        pattern = VollbeschaeftigungPlugin.getSelf().getPatternLibrary().load((String) source.get("pattern"));
        data = (Data) source.get("data");
        completedTasks = new HashSet<>((Collection<String>) source.get("completedTasks"));
    }

    /**
     * Constructor.
     *
     * @param pattern The pattern to create a job from.
     */
    public Job(final JobPattern pattern) {
        id = UUID.randomUUID();
        this.pattern = pattern;
        data = pattern.generateData();
        completedTasks = new HashSet<>();
    }

    /**
     * Obtains a list item for this job.
     *
     * @return The list item.
     */
    public ItemStack getListItem() {
        final String titleFormat = Messenger.getFormat("vb.item.listItem.title");
        final String flavorFormat = Messenger.getFormat("vb.item.listItem.flavor");
        final String clientFormat = Messenger.getFormat("vb.item.listItem.client");
        final String ratingFormat = Messenger.getFormat("vb.item.listItem.rating");
        final String goalFormat = Messenger.getFormat("vb.item.listItem.goal");
        final String title = String.format(titleFormat, data.getEntry(pattern.getTitleKey(), String.class));
        final String flavor = String.format(flavorFormat, data.getEntry(pattern.getFlavorTextKey(), String.class));
        final String client = String.format(clientFormat, data.getEntry(pattern.getClientKey(), String.class));
        final String rating = String.format(ratingFormat, data.getEntry(pattern.getRatingKey(), String.class));
        final String goal = String.format(goalFormat, data.getEntry(pattern.getGoalTextKey(), String.class));
        return (new ItemFactory())
                .type(Material.PAPER)
                .name(title)
                .lore(flavor, client, rating, goal)
                .ban()
                .instance();
    }

    /**
     * Checks whether or not a task is completed.
     *
     * @param task The task identifier.
     * @return {@code true} iff the task is completed.
     */
    public boolean isTaskCompleted(final String task) {
        return completedTasks.contains(task);
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new HashMap<>();
        result.put("id", id.toString());
        result.put("pattern", pattern.getPatternId());
        result.put("data", data);
        result.put("completedTasks", new ArrayList<>(completedTasks));
        return result;
    }
}
