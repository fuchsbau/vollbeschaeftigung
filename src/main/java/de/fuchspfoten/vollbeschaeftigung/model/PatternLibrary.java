package de.fuchspfoten.vollbeschaeftigung.model;

/**
 * The pattern library keeps the patterns from the patterns/ subdirectory.
 */
public class PatternLibrary extends BaseLibrary<JobPattern> {

    /**
     * Constructor.
     */
    public PatternLibrary() {
        super(JobPattern.class);
    }

    @Override
    public JobPattern load(final String id) {
        return super.load(id);
    }
}
