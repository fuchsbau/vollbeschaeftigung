package de.fuchspfoten.vollbeschaeftigung.model;

import java.util.function.Function;

/**
 * Something which can have its contained keys transformed.
 *
 * @param <T> Should be the implementing class.
 */
public interface KeyTransformable<T extends KeyTransformable<T>> {

    /**
     * Transforms the contained keys in accordance with the given key transformer.
     *
     * @param keyTransformer The key transformer.
     * @return The transformed object.
     */
    T transformKeys(Function<String, String> keyTransformer);
}
