package de.fuchspfoten.vollbeschaeftigung.model;

import de.fuchspfoten.vollbeschaeftigung.model.table.Table;
import lombok.Getter;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A job pattern describes the common properties of jobs of this pattern.
 */
@SerializableAs("dfv.JobPattern")
public class JobPattern implements ConfigurationSerializable {

    /**
     * The ID of the pattern.
     */
    private @Getter final String patternId;

    /**
     * The data table for this job pattern.
     */
    private final Table dataTable;

    /**
     * The data key for the title of the job.
     */
    private @Getter final String titleKey;

    /**
     * The data key for the flavor text of the job.
     */
    private @Getter final String flavorTextKey;

    /**
     * The data key for the client of the job.
     */
    private @Getter final String clientKey;

    /**
     * The data key for the rating of the job.
     */
    private @Getter final String ratingKey;

    /**
     * The goal text data key of the job.
     */
    private @Getter final String goalTextKey;

    /**
     * The data key for the reward of the job.
     */
    private final String rewardKey;

    /**
     * The data key for the condition for accepting the job.
     */
    private final String conditionKey;

    /**
     * The data keys for the tasks of the job.
     */
    private final List<String> taskKeys;

    /**
     * The data key for the goal condition of the job.
     */
    private final String goalKey;

    /**
     * Deserialization constructor.
     *
     * @param source Source map.
     */
    @SuppressWarnings("unchecked")
    public JobPattern(final Map<String, Object> source) {
        patternId = (String) source.get("id");
        dataTable = (Table) source.get("data");
        titleKey = (String) source.get("title");
        flavorTextKey = (String) source.get("flavorText");
        clientKey = (String) source.get("client");
        ratingKey = (String) source.get("rating");
        rewardKey = (String) source.get("reward");
        conditionKey = (String) source.get("condition");
        taskKeys = (List<String>) source.get("tasks");
        goalKey = (String) source.get("goal");
        goalTextKey = (String) source.get("goalText");
    }

    /**
     * Randomly generates a data storage for an instance of this job pattern.
     *
     * @return The generated data storage.
     */
    public Data generateData() {
        final Data result = new Data();
        dataTable.pick(result);
        result.evaluateData();
        return result;
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new HashMap<>();
        result.put("id", patternId);
        result.put("data", dataTable);
        result.put("title", titleKey);
        result.put("flavorText", flavorTextKey);
        result.put("client", clientKey);
        result.put("rating", ratingKey);
        result.put("reward", rewardKey);
        result.put("condition", conditionKey);
        result.put("tasks", taskKeys);
        result.put("goal", goalKey);
        result.put("goalText", goalTextKey);
        return result;
    }
}
