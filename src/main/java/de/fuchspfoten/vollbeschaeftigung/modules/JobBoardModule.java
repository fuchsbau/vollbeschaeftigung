package de.fuchspfoten.vollbeschaeftigung.modules;

import de.fuchspfoten.vollbeschaeftigung.model.BaseLibrary;
import de.fuchspfoten.vollbeschaeftigung.model.board.JobBoard;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * The job board module manages job boards and interactions with them.
 */
public class JobBoardModule extends BaseLibrary<JobBoard> implements Listener {

    /**
     * Maps player IDs to open job boards.
     */
    private final Map<UUID, JobBoard> openBoards = new HashMap<>();

    /**
     * Constructor.
     */
    public JobBoardModule() {
        super(JobBoard.class);
    }

    /**
     * Checks whether the job board with the given ID exists.
     *
     * @param id The ID.
     * @return {@code true} iff the board exists.
     */
    public boolean hasBoard(final String id) {
        return load(id) != null;
    }

    /**
     * Opens the board with the given ID for the given player.
     *
     * @param id     The board ID.
     * @param player The player.
     */
    public void openBoard(final String id, final HumanEntity player) {
        final JobBoard board = load(id);
        if (board == null) {
            throw new IllegalArgumentException("unknown board id " + id);
        }

        openBoards.put(player.getUniqueId(), board);
        board.openBoard(player);
    }

    @EventHandler
    public void onInventoryClick(final InventoryClickEvent event) {
        if (openBoards.containsKey(event.getWhoClicked().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInventoryDrag(final InventoryDragEvent event) {
        if (openBoards.containsKey(event.getWhoClicked().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInventoryClose(final InventoryCloseEvent event) {
        openBoards.remove(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onPlayerQuit(final PlayerQuitEvent event) {
        openBoards.remove(event.getPlayer().getUniqueId());
    }
}
