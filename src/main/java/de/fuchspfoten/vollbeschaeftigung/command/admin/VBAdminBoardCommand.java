package de.fuchspfoten.vollbeschaeftigung.command.admin;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.vollbeschaeftigung.VollbeschaeftigungPlugin;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /vb admin board &lt;board&gt; command.
 */
public class VBAdminBoardCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public VBAdminBoardCommand() {
        super("vb.vbHelp.admin.board", "vb.admin");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "vb.vbHelp.admin.board");
            return;
        }
        final String boardId = args[0];

        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        if (!VollbeschaeftigungPlugin.getSelf().getJobBoardModule().hasBoard(boardId)) {
            sender.sendMessage("Invalid board");
            return;
        }
        VollbeschaeftigungPlugin.getSelf().getJobBoardModule().openBoard(boardId, player);
    }
}
