package de.fuchspfoten.vollbeschaeftigung.command.admin;

import de.fuchspfoten.fuchslib.command.TreeCommand;

/**
 * /vb admin command.
 */
public class VBAdminCommand extends TreeCommand {

    /**
     * Constructor.
     */
    public VBAdminCommand() {
        super(null);  // No helpline to prevent listing the command.
        addSubCommand("board", new VBAdminBoardCommand());
        addSubCommand("sample", new VBAdminSampleCommand());
    }
}
