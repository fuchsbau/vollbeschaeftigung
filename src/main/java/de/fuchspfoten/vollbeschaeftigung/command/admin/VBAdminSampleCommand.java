package de.fuchspfoten.vollbeschaeftigung.command.admin;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import de.fuchspfoten.fuchslib.command.LeafCommand;
import de.fuchspfoten.vollbeschaeftigung.VollbeschaeftigungPlugin;
import de.fuchspfoten.vollbeschaeftigung.model.Job;
import de.fuchspfoten.vollbeschaeftigung.model.JobPattern;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

/**
 * /vb admin sample &lt;pattern&gt; command.
 */
public class VBAdminSampleCommand extends LeafCommand {

    /**
     * Constructor.
     */
    public VBAdminSampleCommand() {
        super("vb.vbHelp.admin.sample", "vb.admin");
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        // Syntax check.
        if (args.length != 1) {
            Messenger.send(sender, "vb.vbHelp.admin.sample");
            return;
        }
        final String patternId = args[0];

        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return;
        }

        final JobPattern pattern = VollbeschaeftigungPlugin.getSelf().getPatternLibrary().load(patternId);
        if (pattern == null) {
            sender.sendMessage("Invalid pattern");
            return;
        }

        final Inventory sampleInv = Bukkit.createInventory(null, 54, "Sample");
        for (int i = 0; i < sampleInv.getSize(); i++) {
            final Job sample = new Job(pattern);
            sampleInv.setItem(i, sample.getListItem());
        }
        player.openInventory(sampleInv);
    }
}
