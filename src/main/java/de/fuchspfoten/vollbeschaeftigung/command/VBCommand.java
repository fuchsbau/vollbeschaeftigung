package de.fuchspfoten.vollbeschaeftigung.command;

import de.fuchspfoten.fuchslib.command.TreeCommand;
import de.fuchspfoten.vollbeschaeftigung.command.admin.VBAdminCommand;

/**
 * /vb command.
 */
public class VBCommand extends TreeCommand {

    /**
     * Constructor.
     */
    public VBCommand() {
        super(null);
        addSubCommand("admin", new VBAdminCommand());
    }
}
