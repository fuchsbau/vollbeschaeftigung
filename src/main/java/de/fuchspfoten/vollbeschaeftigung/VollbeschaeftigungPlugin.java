package de.fuchspfoten.vollbeschaeftigung;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.vollbeschaeftigung.command.VBCommand;
import de.fuchspfoten.vollbeschaeftigung.model.BaseLibrary;
import de.fuchspfoten.vollbeschaeftigung.model.Data;
import de.fuchspfoten.vollbeschaeftigung.model.Job;
import de.fuchspfoten.vollbeschaeftigung.model.JobPattern;
import de.fuchspfoten.vollbeschaeftigung.model.PatternLibrary;
import de.fuchspfoten.vollbeschaeftigung.model.board.JobBoard;
import de.fuchspfoten.vollbeschaeftigung.model.dataobject.Formula;
import de.fuchspfoten.vollbeschaeftigung.model.dataobject.IntRange;
import de.fuchspfoten.vollbeschaeftigung.model.dataobject.condition.JobListCondition;
import de.fuchspfoten.vollbeschaeftigung.model.dataobject.condition.TaskDNFCondition;
import de.fuchspfoten.vollbeschaeftigung.model.dataobject.condition.TrueCondition;
import de.fuchspfoten.vollbeschaeftigung.model.dataobject.reward.MoneyReward;
import de.fuchspfoten.vollbeschaeftigung.model.dataobject.task.NPCDeliveryTask;
import de.fuchspfoten.vollbeschaeftigung.model.table.CombineTable;
import de.fuchspfoten.vollbeschaeftigung.model.table.PickTable;
import de.fuchspfoten.vollbeschaeftigung.model.table.RefTable;
import de.fuchspfoten.vollbeschaeftigung.model.table.TableLibrary;
import de.fuchspfoten.vollbeschaeftigung.model.table.ValTable;
import de.fuchspfoten.vollbeschaeftigung.modules.JobBoardModule;
import lombok.Getter;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.logging.Level;

/**
 * The main plugin class.
 */
public class VollbeschaeftigungPlugin extends JavaPlugin {

    /**
     * The singleton plugin instance.
     */
    private @Getter static VollbeschaeftigungPlugin self;

    /**
     * The table library.
     */
    private @Getter TableLibrary tableLibrary;

    /**
     * The job pattern library.
     */
    private @Getter PatternLibrary patternLibrary;

    /**
     * The job board library.
     */
    private @Getter JobBoardModule jobBoardModule;

    /**
     * Creates a directory inside the dataold folder.
     *
     * @param name The name of the directory.
     */
    private void createDirectory(final String name) {
        final File file = new File(getDataFolder(), name);
        if (!file.exists()) {
            if (!file.mkdir()) {
                throw new IllegalStateException("could not create directory: " + name);
            }
        }
    }

    /**
     * Loads a library.
     *
     * @param dir          The directory to load the library from.
     * @param library The library.
     */
    private void loadLibrary(final String dir, final BaseLibrary<?> library) {
        // Ensure the directory is created
        createDirectory(dir);

        // Load the library.
        final File directory = new File(getDataFolder(), dir);
        final File[] dirEntries = directory.listFiles();
        if (dirEntries != null) {
            for (final File entry : dirEntries) {
                if (entry.isDirectory() || !entry.getName().endsWith("yml") || entry.getName().startsWith(".")) {
                    continue;
                }

                try {
                    library.loadFile(entry);
                    getLogger().info("Loaded file for library '" + dir + "' from " + entry.getName());
                } catch (final RuntimeException ex) {
                    getLogger().log(Level.WARNING, "Ignoring file " + entry.getAbsolutePath()
                            + " due to exception", ex);
                }
            }
        } else {
            getLogger().severe("Library directory '" + dir + "' can not be read!");
        }
    }

    @Override
    public void onEnable() {
        self = this;

        // Register serialization classes.
        ConfigurationSerialization.registerClass(CombineTable.class);
        ConfigurationSerialization.registerClass(PickTable.class);
        ConfigurationSerialization.registerClass(RefTable.class);
        ConfigurationSerialization.registerClass(ValTable.class);

        ConfigurationSerialization.registerClass(MoneyReward.class);

        ConfigurationSerialization.registerClass(JobListCondition.class);
        ConfigurationSerialization.registerClass(TaskDNFCondition.class);
        ConfigurationSerialization.registerClass(TrueCondition.class);

        ConfigurationSerialization.registerClass(NPCDeliveryTask.class);

        ConfigurationSerialization.registerClass(Formula.class);
        ConfigurationSerialization.registerClass(IntRange.class);

        ConfigurationSerialization.registerClass(Data.class);
        ConfigurationSerialization.registerClass(Job.class);
        ConfigurationSerialization.registerClass(JobBoard.class);
        ConfigurationSerialization.registerClass(JobPattern.class);

        // Register messages.
        Messenger.register("vb.item.listItem.title");
        Messenger.register("vb.item.listItem.flavor");
        Messenger.register("vb.item.listItem.client");
        Messenger.register("vb.item.listItem.rating");
        Messenger.register("vb.item.listItem.goal");

        // Create the default configuration.
        getConfig().options().copyDefaults(true);
        saveConfig();

        // Load the table library.
        tableLibrary = new TableLibrary();
        loadLibrary("tables", tableLibrary);

        // Load the pattern library.
        patternLibrary = new PatternLibrary();
        loadLibrary("patterns", patternLibrary);

        // Load the board module.
        jobBoardModule = new JobBoardModule();
        loadLibrary("boards", jobBoardModule);
        getServer().getPluginManager().registerEvents(jobBoardModule, this);

        // Register command executor modules.
        getCommand("vb").setExecutor(new VBCommand());
    }
}
